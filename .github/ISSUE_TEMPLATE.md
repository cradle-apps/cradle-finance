Please, submit only real issues. Use the forum for support, feature requests, proposals, new versions, help etc. https://usecradleapps.com/forum

### Steps to reproduce the issue



### Expected result



### Actual result



### System information (Cradle Finance, PHP versions)



### Additional comments


